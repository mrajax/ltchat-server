function ckeckUsername(name) {
  const pattern = /^[a-z][a-z0-9]+$/i
  const test = pattern.test(name)
  const nameLength = name.toString().length

  return test && (nameLength >= 3 && nameLength <= 15)
}

module.exports = server => {
  const io = require('socket.io')(server)
  const chat = io.of('/chat')

  /**
   * Объект пользователей (ключ-имя пользователя - значение soket id клиента)
   * @type {{}}
   */
  const users = {}

  /**
   * Проверка имени пользователя
   * @todo Сделать проверку авторизации пользователя по JWT токену
   * @param socket - инстанс текущего клиента (пользователя)
   * @param next - колбэк, позволяющий перейти к следущему обработчику
   */
  chat.use((socket, next) => {
    const { username } = socket.handshake.query

    if (ckeckUsername(username)) {
      return next()
    }

    next(new Error('Username invalid'))
  })

  chat.on('connection', socket => {
    const { username } = socket.handshake.query

    users[username] = socket.id

    /**
     * При подключении клиента шлём ему массив имён подключенных пользователей
     */
    chat.emit('users online join', Object.keys(users))

    /**
     * Получаем сообщение от клиента и рассылаем всем клиентам
     */
    socket.on('message to all', ({ message }) => {
      chat.emit('message to all', { username, message })
    })

    /**
     * Получаем приватное сообщение и отправляем его клиенту с логином,
     * указанном в параметре коллбэка `to`
     */
    socket.on('private message', ({ to, message }) => {
      chat
        .to(users[to])
        .to(socket.id)
        .emit('private message', { username, to, message })
    })

    /**
     * Обработчик отключения клиента
     * При отключении клиента удаляем его из объекта пользователей `users`
     */
    socket.on('disconnect', () => {
      delete users[username]

      // todo: Сделать так, чтобы при отключении клиента отправлялся только его логин, а дальше уже чтобы клиент этим занимался
      chat.emit('users online join', Object.keys(users))
    })
  })
}
