const { JwtService } = require('../services')
const { User } = require('../modules/users/models')

module.exports = () => async (ctx, next) => {
  const { authorization: token } = ctx.request.headers

  if (token) {
    const errorTokenInvalid = () => ctx.throw(401, 'Unauthorized. Token invalid')

    try {
      const { email, type } = JwtService.verifyToken(token)

      if (type !== 'access') {
        errorTokenInvalid()
      }

      const user = await User.findByEmail(email)

      if (user) {
        ctx.state.user = user
      }
    } catch (e) {
      errorTokenInvalid()
    }
  }

  await next()
}
