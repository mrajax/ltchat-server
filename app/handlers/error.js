module.exports = () => async (ctx, next) => {
  try {
    await next()
  } catch ({ message, status, name, errors }) {
    switch (name) {
      case 'ValidationError':
        ctx.status = 400
        ctx.body = {
          message,
          errors: Object.values(errors).reduce((fields, field) => ({
            ...fields,
            [field.path]: field.message
          }), {})
        }
        break

      case 'TokenExpiredError':
      case 'JsonWebTokenError':
        ctx.status = 400
        ctx.body = {
          message: 'Token invalid'
        }
        break

      default:
        ctx.status = status || 500
        ctx.body = { message }
    }
  }
}
