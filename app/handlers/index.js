const compose = require('koa-compose')
const bodyParser = require('koa-bodyparser')
const logger = require('koa-logger')
const cors = require('@koa/cors')
const helmet = require('koa-helmet')
const jwt = require('./jwt')
const error = require('./error')

module.exports = compose([
  bodyParser(),
  helmet(),
  logger(),
  cors(),
  jwt(),
  error()
])
