const http = require('http')
const debug = require('debug')('ltchat-server:server')
const config = require('./config')
const app = require('./app')
const socket = require('./sockets')
const mongooseConnect = require('./libs/mongoose')

const server = http.createServer(app.callback())
const { port, host } = config.server

function startServer() {
  server.listen({ port, host }, err => {
    if (err) {
      debug('Error starting server', err)
      process.exit(1)
    }

    socket(server)

    debug(`Server started on address http://${host}:${port}`)
  })
}

(function startDatabase() {
  mongooseConnect()
    .on('error', err => debug(err))
    .on('connected', () => debug('MongoDB connected'))
    .on('disconnected', () => server.close(() => {
      startDatabase()
    }))
    .once('open', () => startServer())
})()

module.exports = server
