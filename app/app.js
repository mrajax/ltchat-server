const Koa = require('koa')
const handlers = require('./handlers')
const modules = require('./modules')

const app = new Koa()

app.use(handlers)
app.use(modules)

module.exports = app
