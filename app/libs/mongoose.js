const mongoose = require('mongoose')
const Blurbird = require('bluebird')
const debug = require('debug')('ltchat-server:mongoose')
const config = require('../config')

mongoose.Promise = Blurbird

const { host, port, name } = config.mongodb
const mongoUri = `mongodb://${host}:${port}/${name}`

module.exports = () => {
  mongoose.connect(mongoUri, { useNewUrlParser: true }).catch(err => debug(err))
  return mongoose.connection
}
