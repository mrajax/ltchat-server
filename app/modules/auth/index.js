const Router = require('koa-router')
const authController = require('./controllers/auth-controller')
const { authorize } = require('../../handlers/polices')

const router = new Router({ prefix: '/auth' })

router
  .get('/user', authorize(), authController.currentUser)
  .post('/signin', authController.signIn)
  .post('/refresh-tokens', authController.refreshTokens)

module.exports = router.routes()
