const config = require('../../../config')
const { JwtService } = require('../../../services')
const { User } = require('../../users/models')

module.exports = {
  async getAuthTokens(user) {
    const { email, _id } = user
    const { accessExpiresIn, refreshExpiresIn } = config.jwt

    const accessToken = JwtService.generateToken({ email, type: 'access' }, accessExpiresIn)
    const refreshToken = JwtService.generateToken({ email, type: 'refresh' }, refreshExpiresIn)

    await User.findByIdAndUpdate(_id, { refreshToken })

    return {
      accessToken,
      refreshToken
    }
  }
}
