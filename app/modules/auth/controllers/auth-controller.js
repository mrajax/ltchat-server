const { User } = require('../../users/models')
const { AuthService } = require('../services')
const { JwtService } = require('../../../services')

module.exports = {
  /**
   * Аутентификация пользователя
   * @api POST /api/auth/signin
   * @apiParamsBody username, password
   * @param ctx
   * @returns {Promise<void>}
   * @example {
      "accessToken": "...",
      "refreshToken": "..."
   }
   */
  async signIn(ctx) {
    const { username, password } = ctx.request.body

    if (!username || !password) {
      ctx.throw(400, 'Incorrect data')
    }

    const user = await User.findByUsername(username)

    if (!user || !user.comparePasswords(password)) {
      ctx.throw(400, 'Incorrect data')
    }

    ctx.body = await AuthService.getAuthTokens(user)
  },

  /**
   * Возврат новой пары JWT токенов
   * @api POST /api/auth/refresh-tokens
   * @apiParamsBody refreshToken
   * @param ctx
   * @returns {Promise<void>}
   * @example {
      "accessToken": "...",
      "refreshToken": "..."
   }
   */
  async refreshTokens(ctx) {
    try {
      const { refreshToken } = ctx.request.body
      const { email, type } = JwtService.verifyToken(refreshToken)

      if (type !== 'refresh') {
        ctx.throw(400, 'Refresh token invalid')
      }

      const user = await User.findOne({ email, refreshToken })

      if (user) {
        ctx.body = await AuthService.getAuthTokens(user)
      }
    } catch (e) {
      ctx.throw(400, 'Refresh token invalid')
    }
  },

  /**
   * Возврат текущего пользователя
   * @api GET /api/auth/user
   * @apiParamsHeader Authorization
   * @param ctx
   * @returns {Promise<void>}
   */
  async currentUser(ctx) {
    const user = ctx.state.user

    if (!user) {
      ctx.throw(400, 'User does not exist')
    }

    ctx.body = {
      data: user
    }
  }
}
