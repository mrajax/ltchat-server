const mongoose = require('mongoose')
const Isemail = require('isemail')
const bcrypt = require('bcrypt')
const uniqueValidator = require('mongoose-unique-validator')

const validators = {
  async username(value) {
    const user = await this.constructor.findByUsername(value)
    return !user
  },

  email(value) {
    return Isemail.validate(value)
  }
}

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    trim: true,
    required: 'Username not specified',
    minlength: [3, 'Username must be at least 3 characters'],
    maxlength: [15, 'Username must be no more than 15 characters'],
    match: [/^[a-z][a-z0-9]+$/i, 'The username can only consist of Latin letters and numbers and must start with a letter'],
    validate: [validators.username, 'The username must be unique']
  },
  password: {
    type: String,
    required: 'Password not specified'
  },
  email: {
    type: String,
    trim: true,
    required: 'E-mail not specified',
    unique: 'E-mail address must be unique',
    lowercase: true,
    validate: [validators.email, 'E-mail address entered is incorrect']
  },
  refreshToken: {
    type: String
  },
  ipRegistration: {
    type: String,
    required: 'IP-address not transmitted'
  }
}, {
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      delete ret.__v
      delete ret.password
      delete ret.refreshToken
    }
  }
})

UserSchema.plugin(uniqueValidator)

UserSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10)
    this.password = bcrypt.hashSync(this.password, salt)
  }

  next()
})

UserSchema.statics = {
  findByUsername(username) {
    return this.findOne({ username: new RegExp(`^${username}$`, 'i') })
  },

  findByEmail(email) {
    return this.findOne({ email })
  }
}

UserSchema.methods = {
  comparePasswords(password) {
    return bcrypt.compareSync(password, this.password)
  }
}

module.exports = mongoose.model('User', UserSchema)
