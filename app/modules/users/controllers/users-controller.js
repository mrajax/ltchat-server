const { User } = require('../models')

module.exports = {
  /**
   * Создание пользователя
   * @api POST /api/users
   * @apiParamsBody username, password, email
   * @param ctx
   * @returns {Promise<void>}
   * @example {
       data: {
        "_id": "5b86e1e2f046240954d9cdbe",
        "username": "Bob",
        "email": "example@example.com",
        "ipRegistration": "8.8.8.8",
        "createdAt": "2018-08-29T18:11:47.313Z",
        "updatedAt": "2018-08-29T18:11:47.313Z"
       }
   }
   */
  async createUser(ctx) {
    const { username, password, email } = ctx.request.body

    if (!username || !password || !email) {
      ctx.throw(400, 'Incorrect data')
    }

    const ip = ctx.ip

    const user = await User.create({ username, password, email, ipRegistration: ip })

    ctx.status = 201
    ctx.body = { data: user }
  },

  /**
   * Возврат пользователей
   * @api GET /api/users?offset=10&limit=100
   * @apiParamsQuery offset, limit
   * @param ctx
   * @returns {Promise<void>}
   * @example {
      "count": 2,
      "offset": 0,
      "limit": 1,
      "data": [
         {
            "_id": "5b5e3f057129ab23d06ef321",
            "username": "Bob",
            "email": "example@example.com",
            "ipRegistration": "8.8.8.8",
            "createdAt": "2018-07-29T22:26:13.928Z",
            "updatedAt": "2018-07-29T23:09:55.456Z"
         }
       ]
    }
   */
  async getUsers(ctx) {
    const { offset, limit } = ctx.request.query

    const offsetNormalize = parseInt(offset, 10) || 0
    const limitNormalize = parseInt(limit, 10) || 30

    if (limitNormalize > 100) {
      ctx.throw(400, 'Limit is exceeded')
    }

    const users = await User.find()
      .skip(offsetNormalize)
      .limit(limitNormalize)

    const count = await User.count()

    ctx.body = {
      count,
      offset: offsetNormalize,
      limit: limitNormalize,
      data: users
    }
  }
}
