const Router = require('koa-router')
const usersController = require('./controllers/users-controller')
const { authorize } = require('../../handlers/polices')

const router = new Router({ prefix: '/users' })

router
  .get('/', authorize(), usersController.getUsers)
  .post('/', usersController.createUser)

module.exports = router.routes()
