const dotenv = require('dotenv')

dotenv.config()

module.exports = Object.freeze({
  environment: process.env.NODE_ENV || process.env.ENV || 'development',
  server: {
    port: process.env.PORT || 3000,
    host: process.env.HOST || 'localhost'
  },
  mongodb: {
    host: process.env.MONGO_DB_HOST || 'localhost',
    port: process.env.MONGO_DB_PORT || 27017,
    name: process.env.MONGO_DB_NAME || 'ltchat'
  },
  jwt: {
    secret: process.env.JWT_SECRET || 'ltchat-jwt-secret',
    accessExpiresIn: process.env.JWT_ACCESS_EXPIRES_IN || 3600,
    refreshExpiresIn: process.env.JWT_REFRESH_EXPIRES_IN || 5184000
  }
})
