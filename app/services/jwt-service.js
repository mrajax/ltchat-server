const jwt = require('jsonwebtoken')
const config = require('../config')

const { secret } = config.jwt

module.exports = {
  verifyToken(token) {
    return jwt.verify(token, secret)
  },

  generateToken(payload, exp) {
    return jwt.sign(payload, secret, { expiresIn: parseInt(exp) })
  }
}
